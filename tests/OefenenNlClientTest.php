<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Types\Url;
use PHPUnit\Framework\TestCase;

/**
 * Class ClientTest
 * @package Hyperized\OefenenNlApi
 */
class OefenenNlClientTest extends TestCase
{
    private OefenenNlClient $instance;

    protected function setUp(): void
    {
        parent::setUp();
        $this->instance = OefenenNlClient::new(
            Url::fromString('test://oefenen.nl/'),
            true,
            true
        );
    }

    public function testInvalidUrl(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Url::fromString('!?Invalid');
    }

    public function testGuzzleClient(): void
    {
        self::assertIsObject(
            $this->instance->getHttpClient()
        );
    }

    public function testCanGetContainer(): void
    {
        self::assertIsArray(
            $this->instance->getContainer()
        );
    }

    public function testCanGetStack(): void
    {
        self::assertIsObject(
            $this->instance->getStack()
        );
    }
}