<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\OefenenNlClient;
use Hyperized\OefenenNlApi\Responses\Close as CloseReponse;
use Hyperized\OefenenNlApi\Responses\ConfirmCredentials as ConfirmCredentialsResponse;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Ticket;
use Hyperized\OefenenNlApi\Types\Url;
use Hyperized\OefenenNlApi\Types\UserKey;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfirmCredentialsTest
 * @package Hyperized\OefenenNlApi\Actions
 */
class ConfirmCredentialsTest extends TestCase
{
    protected static string $testUsername = 'Test User';
    protected static string $testGroup = 'Test Group';
    protected Serializer $serializer;
    protected ConfirmCredentials $instance;
    private AbstractEnvelope $confirmResponseGood;
    private AbstractEnvelope $confirmResponseBad;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = SerializerBuilder
            ::create()
            ->build();
        $this->instance = ConfirmCredentials::new(
            OefenenNlClient::new(
                Url::fromString('test://oefenen.nl/')
            ),
            $this->serializer,
            ProgramKey::fromString('abcdefghijklmnopqrstuvwxyz0123456789'),
            Ticket::fromString('12345'),
            UserKey::fromString('12345')
        );

        $this->confirmResponseGood = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'confirm_credentials_good.xml'),
                ConfirmCredentialsResponse::class
            );

        $this->confirmResponseBad = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'confirm_credentials_bad.xml'),
                ConfirmCredentialsResponse::class
            );
    }

    public function testInstanceOf(): void
    {
        self::assertInstanceOf(AbstractAction::class, $this->instance);
        self::assertInstanceOf(AbstractEnvelope::class, $this->confirmResponseGood);
        self::assertInstanceOf(AbstractEnvelope::class, $this->confirmResponseBad);
    }

    public function testCanDeserializeResponse(): void
    {
        /** @var ConfirmCredentialsResponse $confirm */
        $confirm = $this->confirmResponseGood;

        self::assertSame(
            static::$testUsername,
            $confirm->getUser()->getUserName()
        );

        self::assertSame(
            static::$testGroup,
            $confirm->getUser()->getGroup()
        );
    }

    public function testCanValidate(): void
    {
        self::assertIsObject(
            $this->instance::validate(
                $this->confirmResponseGood
            )
        );
    }

    public function testCanFailValidation(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::confirmationFailed());

        $this->instance::validate(
            $this->confirmResponseBad
        );
    }

    public function testCanHaveWrongType(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::incorrectResponseType());

        $this->instance::validate(
            $this->instance::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'confirm_credentials_good.xml'),
                CloseReponse::class
            )
        );
    }
}
