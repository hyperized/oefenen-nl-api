<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Utils;
use Hyperized\OefenenNlApi\AbstractClient;
use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Types\HttpBody;
use Hyperized\OefenenNlApi\Types\HttpMethod;
use Hyperized\OefenenNlApi\Types\Query;
use Hyperized\OefenenNlApi\Types\ResponseClass;
use Hyperized\OefenenNlApi\Types\Url;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractActionTest
 * @package Hyperized\OefenenNlApi\Actions
 */
class AbstractActionTest extends TestCase
{
    private AbstractAction $instance;
    private AbstractClient $client;
    private AbstractClient $failRequestClient;

    protected function setUp(): void
    {
        parent::setUp();

        /** @var SerializerInterface $serializer */
        $serializer = new class implements SerializerInterface {
            /**
             * @param $data
             * @param string $format
             * @param SerializationContext|null $context
             * @param string|null $type
             * @return string
             */
            public function serialize($data, string $format, ?SerializationContext $context = null, ?string $type = null): string
            {
                return 'Some data yo';
            }

            /**
             * @param string $data
             * @param string $type
             * @param string $format
             * @param DeserializationContext|null $context
             * @return AbstractEnvelope
             */
            public function deserialize(string $data, string $type, string $format, ?DeserializationContext $context = null)
            {
                return (new class extends AbstractEnvelope {
                    public function myClassName(): string
                    {
                        return self::class;
                    }
                });
            }
        };

        /** @var AbstractClient client */
        $this->client = new class extends AbstractClient {
            public static function new(Url $url): HttpClientInterface
            {
                return new static();
            }

            public function getHttpClient(): GuzzleClient
            {
                return new GuzzleClient([
                    'handler' => HandlerStack::create(
                        new MockHandler(
                            [
                                new Response(200),
                            ]
                        )
                    )
                ]);
            }
        };

        /** @var AbstractClient failRequestClient */
        $this->failRequestClient = new class extends AbstractClient {
            public static function new(Url $url): HttpClientInterface
            {
                return new static();
            }

            public function getHttpClient(): GuzzleClient
            {
                return new GuzzleClient([
                    'handler' => HandlerStack::create(
                        new MockHandler(
                            [
                                new RequestException(
                                    'Error Communicating with Server',
                                    new Request('GET', 'test')
                                )
                            ]
                        )
                    )
                ]);
            }
        };

        /** @var AbstractAction instance */
        $this->instance = new class($this->client, $serializer) extends AbstractAction {
            public function __construct(AbstractClient $client, SerializerInterface $serializer)
            {
                $this->client = $client;
                $this->serializer = $serializer;
                $this->query = Query::fromArray();
                $this->httpMethod = HttpMethod::fromString('GET');
                $this->stream = HttpBody::fromString('Hello World!');
                $this->responseClass = ResponseClass::fromString(self::class);
            }

            public static function validate(AbstractEnvelope $response): AbstractEnvelope
            {
                return new class extends AbstractEnvelope {
                };
            }

            public static function getResponseType(): string
            {
                return (new class extends AbstractEnvelope {
                    public function myClassName(): string
                    {
                        return self::class;
                    }
                })->myClassName();
            }
        };
    }

    public function testCanExecute(): void
    {
        self::assertIsObject(
            $this->instance->execute()
        );
    }

    public function testCanGetResponseBody(): void
    {
        self::assertIsObject(
            $this->instance::getResponseBody(
                new Response(200, [], 'Hello world!')
            )
        );
    }

    public function testCanGetResponse(): void
    {
        self::assertIsObject(
            $this->instance::getResponse(
                $this->client,
                $this->instance::getRequest(
                    HttpMethod::fromString('GET'),
                    Utils::streamFor('Hello world')
                ),
                Query::fromArray()
            )
        );
    }

    public function testCantGetResponse(): void
    {
        $this->expectException(InvalidArgumentException::class);

        self::assertIsObject(
            $this->instance::getResponse(
                $this->failRequestClient,
                $this->instance::getRequest(
                    HttpMethod::fromString('GET'),
                    Utils::streamFor('Hello world')
                ),
                Query::fromArray()
            )
        );
    }

    public function testCanGetRequest(): void
    {
        self::assertIsObject(
            $this->instance::getRequest(
                HttpMethod::fromString('GET'),
                Utils::streamFor('')
            )
        );
    }

    public function testCanGetPsr7Stream(): void
    {
        self::assertIsObject(
            $this->instance::getPsr7Stream(
                HttpBody::fromString('Hello world!')
            )
        );
    }
}
