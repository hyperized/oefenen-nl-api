<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\OefenenNlClient;
use Hyperized\OefenenNlApi\Responses\Close as CloseResponse;
use Hyperized\OefenenNlApi\Responses\ConfirmCredentials as ConfirmCredentialsResponse;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Ticket;
use Hyperized\OefenenNlApi\Types\Url;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class CloseTest
 * @package Hyperized\OefenenNlApi\Actions
 */
class CloseTest extends TestCase
{
    protected Close $instance;
    protected Serializer $serializer;
    private AbstractEnvelope $closeResponseGood;
    private AbstractEnvelope $closeResponseBad;

    protected function setUp(): void
    {
        parent::setUp();

        $this->serializer = SerializerBuilder
            ::create()
            ->build();
        $this->instance = Close::new(
            OefenenNlClient::new(
                Url::fromString('test://oefenen.nl/')
            ),
            $this->serializer,
            ProgramKey::fromString('abcdefghijklmnopqrstuvwxyz0123456789'),
            Ticket::fromString('12345')
        );

        $this->closeResponseGood = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'close_good.xml'),
                CloseResponse::class
            );

        $this->closeResponseBad = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'close_bad.xml'),
                CloseResponse::class
            );
    }

    public function testInstanceOf(): void
    {
        self::assertInstanceOf(AbstractAction::class, $this->instance);
        self::assertInstanceOf(AbstractEnvelope::class, $this->closeResponseGood);
        self::assertInstanceOf(AbstractEnvelope::class, $this->closeResponseBad);
    }

    public function testCanDeserializeResponse(): void
    {
        /** @var CloseResponse $close */
        $close = $this->closeResponseGood;

        self::assertTrue(
            $close->getResult()
        );
    }

    public function testCanValidate(): void
    {
        self::assertIsObject(
            $this->instance::validate(
                $this->closeResponseGood
            )
        );
    }

    public function testCanFailValidation(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::closeFailed());

        $this->instance::validate(
            $this->closeResponseBad
        );
    }

    public function testCanHaveWrongType(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::incorrectResponseType());

        $this->instance::validate(
            $this->instance::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'close_good.xml'),
                ConfirmCredentialsResponse::class
            )
        );
    }
}
