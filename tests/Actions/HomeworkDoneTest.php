<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\OefenenNlClient;
use Hyperized\OefenenNlApi\Responses\Close as CloseResponse;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\HomeworkDone as HomeworkDoneResponse;
use Hyperized\OefenenNlApi\Types\ChapterId;
use Hyperized\OefenenNlApi\Types\ModuleId;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Url;
use Hyperized\OefenenNlApi\Types\UserKey;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class HomeworkDoneTest
 * @package Hyperized\OefenenNlApi\Actions
 */
class HomeworkDoneTest extends TestCase
{
    protected Serializer $serializer;
    protected HomeworkDone $instance;
    private AbstractEnvelope $homeworkResponseGood;
    private AbstractEnvelope $homeworkResponseBad;

    protected function setUp(): void
    {
        parent::setUp();

        $this->serializer = SerializerBuilder
            ::create()
            ->build();
        $this->instance = HomeworkDone::new(
            OefenenNlClient::new(
                Url::fromString('test://oefenen.nl/')
            ),
            $this->serializer,
            ProgramKey::fromString('abcdefghijklmnopqrstuvwxyz0123456789'),
            UserKey::fromString('12345'),
            ChapterId::fromString('12345'),
            ModuleId::fromString('12345')
        );

        $this->homeworkResponseGood = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'homework_done_good.xml'),
                HomeworkDoneResponse::class
            );

        $this->homeworkResponseBad = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'homework_done_bad.xml'),
                HomeworkDoneResponse::class
            );
    }

    public function testInstanceOf(): void
    {
        self::assertInstanceOf(AbstractAction::class, $this->instance);
        self::assertInstanceOf(AbstractEnvelope::class, $this->homeworkResponseGood);
        self::assertInstanceOf(AbstractEnvelope::class, $this->homeworkResponseBad);
    }

    public function testCanDeserializeResponse(): void
    {
        /** @var HomeworkDoneResponse $homework_done */
        $homework_done = $this->homeworkResponseGood;

        self::assertTrue(
            $homework_done->getResult()
        );
    }

    public function testCanValidate(): void
    {
        self::assertIsObject(
            $this->instance::validate(
                $this->homeworkResponseGood
            )
        );
    }

    public function testCanFailValidation(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::homeworkDoneFailed());

        $this->instance::validate(
            $this->homeworkResponseBad
        );
    }

    public function testCanHaveWrongType(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::incorrectResponseType());

        $this->instance::validate(
            $this->instance::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'homework_done_good.xml'),
                CloseResponse::class
            )
        );
    }
}
