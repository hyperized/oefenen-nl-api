<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\OefenenNlClient;
use Hyperized\OefenenNlApi\Requests\Module;
use Hyperized\OefenenNlApi\Requests\Modules as ModulesRequest;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\Close as CloseResponse;
use Hyperized\OefenenNlApi\Responses\Modules as ModulesResponse;
use Hyperized\OefenenNlApi\Types\ChapterId;
use Hyperized\OefenenNlApi\Types\ChapterTitle;
use Hyperized\OefenenNlApi\Types\ExerciseCount;
use Hyperized\OefenenNlApi\Types\ModuleId;
use Hyperized\OefenenNlApi\Types\ModuleTitle;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Sequence;
use Hyperized\OefenenNlApi\Types\Url;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class ModulesTest
 * @package Hyperized\OefenenNlApi\Actions
 */
class ModulesTest extends TestCase
{
    protected Serializer $serializer;
    protected Modules $instance;
    private AbstractEnvelope $modulesResponseGood;
    private AbstractEnvelope $modulesResponseBad;
    private string $modulesRequestGood;

    protected function setUp(): void
    {
        parent::setUp();

        $request_modules = ModulesRequest::new(
            Module::new(
                ModuleId::fromInteger(1),
                ModuleTitle::fromString('Hello world'),
                ChapterId::fromInteger(1),
                ChapterTitle::fromString('Hello world'),
                ExerciseCount::fromInteger(1),
                Sequence::fromInteger(1)
            ),
            Module::new(
                ModuleId::fromInteger(2),
                ModuleTitle::fromString('Hello world'),
                ChapterId::fromInteger(2),
                ChapterTitle::fromString('Hello world'),
                ExerciseCount::fromInteger(2),
                Sequence::fromInteger(2)
            )
        );

        $this->serializer = SerializerBuilder
            ::create()
            ->build();
        $this->instance = Modules::new(
            OefenenNlClient::new(
                Url::fromString('test://oefenen.nl/')
            ),
            $this->serializer,
            ProgramKey::fromString('abcdefghijklmnopqrstuvwxyz0123456789'),
            $request_modules
        );

        $this->modulesRequestGood = $this->instance
            ::serialize(
                $this->serializer,
                $request_modules
            );

        $this->modulesResponseGood = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'modules_reply_good.xml'),
                ModulesResponse::class
            );

        $this->modulesResponseBad = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'modules_reply_bad.xml'),
                ModulesResponse::class
            );
    }

    public function testInstanceOf(): void
    {
        self::assertInstanceOf(AbstractAction::class, $this->instance);
        self::assertInstanceOf(AbstractEnvelope::class, $this->modulesResponseGood);
        self::assertInstanceOf(AbstractEnvelope::class, $this->modulesResponseBad);
    }

    public function testCanSerializeRequest(): void
    {
        self::assertXmlStringEqualsXmlString(
            $this->modulesRequestGood,
            file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'modules_correct.xml'),
        );
    }

    public function testCanDeserializeResponse(): void
    {
        /** @var ModulesResponse $modules */
        $modules = $this->modulesResponseGood;

        self::assertTrue(
            $modules->getResult()
        );
    }

    public function testCanValidate(): void
    {
        self::assertIsObject(
            $this->instance::validate(
                $this->modulesResponseGood
            )
        );
    }

    public function testCanFailValidation(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::modulesFailed());

        $this->instance::validate(
            $this->modulesResponseBad
        );
    }

    public function testCanHaveWrongType(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::incorrectResponseType());

        $this->instance::validate(
            $this->instance::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'modules_reply_good.xml'),
                CloseResponse::class
            )
        );
    }
}
