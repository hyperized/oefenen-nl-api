<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\OefenenNlClient;
use Hyperized\OefenenNlApi\Requests\Result;
use Hyperized\OefenenNlApi\Requests\Results as ResultsRequest;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\Close as CloseResponse;
use Hyperized\OefenenNlApi\Responses\Results as ResultsResponse;
use Hyperized\OefenenNlApi\Types\ChapterId;
use Hyperized\OefenenNlApi\Types\ExerciseCorrect;
use Hyperized\OefenenNlApi\Types\ExerciseDone;
use Hyperized\OefenenNlApi\Types\ExerciseTotal;
use Hyperized\OefenenNlApi\Types\ModuleId;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Url;
use Hyperized\OefenenNlApi\Types\UserKey;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class ResultsTest
 * @package Hyperized\OefenenNlApi\Actions
 */
class ResultsTest extends TestCase
{
    protected Serializer $serializer;
    protected Results $instance;
    private AbstractEnvelope $resultsResponseGood;
    private AbstractEnvelope $resultsResponseBad;
    private string $resultsRequestGood;

    protected function setUp(): void
    {
        parent::setUp();

        $request_results = ResultsRequest::new(
            Result::new(
                UserKey::fromString('hello'),
                ChapterId::fromInteger(1),
                ModuleId::fromInteger(1),
                ExerciseTotal::fromInteger(10),
                ExerciseDone::fromInteger(10),
                ExerciseCorrect::fromInteger(10),
                Url::fromString('https://inburgeringa1.nl/lessen')
            ),
            Result::new(
                UserKey::fromString('hello'),
                ChapterId::fromInteger(2),
                ModuleId::fromInteger(2),
                ExerciseTotal::fromInteger(10),
                ExerciseDone::fromInteger(10),
                ExerciseCorrect::fromInteger(10),
                Url::fromString('https://inburgeringa1.nl/lessen')
            )
        );

        $this->serializer = SerializerBuilder
            ::create()
            ->build();
        $this->instance = Results::new(
            OefenenNlClient::new(
                Url::fromString('test://oefenen.nl/')
            ),
            $this->serializer,
            ProgramKey::fromString('abcdefghijklmnopqrstuvwxyz0123456789'),
            $request_results
        );

        $this->resultsRequestGood = $this->instance
            ::serialize(
                $this->serializer,
                $request_results
            );

        $this->resultsResponseGood = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'results_reply_good.xml'),
                ResultsResponse::class
            );

        $this->resultsResponseBad = $this->instance
            ::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'results_reply_bad.xml'),
                ResultsResponse::class
            );
    }

    public function testInstanceOf(): void
    {
        self::assertInstanceOf(AbstractAction::class, $this->instance);
        self::assertInstanceOf(AbstractEnvelope::class, $this->resultsResponseGood);
        self::assertInstanceOf(AbstractEnvelope::class, $this->resultsResponseBad);
    }

    public function testCanSerializeRequest(): void
    {
        self::assertXmlStringEqualsXmlString(
            $this->resultsRequestGood,
            file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'results_correct.xml'),
        );
    }

    public function testCanDeserializeResponse(): void
    {
        /** @var ResultsResponse $modules */
        $modules = $this->resultsResponseGood;

        self::assertTrue(
            $modules->getResult()
        );
    }

    public function testCanValidate(): void
    {
        self::assertIsObject(
            $this->instance::validate(
                $this->resultsResponseGood
            )
        );
    }

    public function testCanFailValidation(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::resultsFailed());

        $this->instance::validate(
            $this->resultsResponseBad
        );
    }

    public function testCanHaveWrongType(): void
    {
        $this->expectExceptionObject(InvalidArgumentException::incorrectResponseType());

        $this->instance::validate(
            $this->instance::deserialize(
                $this->serializer,
                file_get_contents('tests' . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'results_reply_good.xml'),
                CloseResponse::class
            )
        );
    }
}
