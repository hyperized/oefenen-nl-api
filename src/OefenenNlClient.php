<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Types\Url;

/**
 * Class OefenenNlClient
 * @package Hyperized\OefenenNlApi
 */
final class OefenenNlClient implements HttpClientInterface
{
    private GuzzleClient $httpClient;
    private float $requestTimeout = 10.0;
    private string $userAgent = 'hyperized/oefenen-nl-api';
    private array $container;
    private HandlerStack $stack;

    protected function __construct(Url $baseUri, bool $debug = false, bool $history = false)
    {
        $this->container = [];
        $this->stack = HandlerStack::create();
        if($history)
        {
            $this->stack->push(Middleware::history($this->container));
        }
        $this->httpClient = (new GuzzleClient(
            [
                'handler' => $this->stack,
                'base_uri' => $baseUri->getValue(),
                'timeout' => $this->requestTimeout,
                'debug' => $debug,
                'headers' => [
                    'User-Agent' => $this->userAgent
                ],
            ]
        ));
    }

    public static function new(Url $url, bool $debug = false, bool $history = false): self
    {
        return new OefenenNlClient($url, $debug, $history);
    }

    public function getHttpClient(): GuzzleClient
    {
        return $this->httpClient;
    }

    public function getContainer(): array
    {
        return $this->container;
    }

    public function getStack(): HandlerStack
    {
        return $this->stack;
    }
}
