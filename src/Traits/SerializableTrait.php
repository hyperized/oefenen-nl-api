<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Traits;

use Hyperized\OefenenNlApi\Requests\AbstractEnvelope as RequestEnvelope;
use JMS\Serializer\SerializerInterface;

/**
 * Class Serializable
 * @package Hyperized\OefenenNlApi\Traits
 */
trait SerializableTrait
{
    /**
     * @param SerializerInterface $serializer
     * @param RequestEnvelope $data
     * @param string $serializationMode
     * @return string
     */
    public static function serialize(
        SerializerInterface $serializer,
        RequestEnvelope $data,
        string $serializationMode = 'xml'
    ): string
    {
        return $serializer->serialize($data, $serializationMode);
    }
}
