<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Traits;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use JMS\Serializer\SerializerInterface;

/**
 * Class Serializable
 * @package Hyperized\OefenenNlApi\Traits
 */
trait DeserializableTrait
{
    /**
     * @param SerializerInterface $serializer
     * @param string $data
     * @param string $type
     * @param string $deserializationMode
     * @return AbstractEnvelope
     * @throws InvalidArgumentException
     */
    public static function deserialize(
        SerializerInterface $serializer,
        string $data,
        string $type,
        string $deserializationMode = 'xml'
    ): AbstractEnvelope
    {
        if ($data === '') {
            return new class extends AbstractEnvelope {
            };
        }

        /** @var AbstractEnvelope $result */
        $result = $serializer
            ->deserialize(
                $data,
                $type,
                $deserializationMode
            );

        return $result;
    }
}
