<?php /** @noinspection EmptyClassInspection */
declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Responses;

use Hyperized\OefenenNlApi\Interfaces\ResponseEnvelopeInterface;

/**
 * Class AbstractEnvelope
 * @package Hyperized\OefenenNlApi\Responses
 */
abstract class AbstractEnvelope implements ResponseEnvelopeInterface
{
}