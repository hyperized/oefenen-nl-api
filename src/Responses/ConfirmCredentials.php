<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Responses;

use JMS\Serializer\Annotation as Serializer;
use Hyperized\OefenenNlApi\Responses\ConfirmCredentialsUser;

/**
 * Class Close
 * @package Hyperized\OefenenNlApi\Responses
 * @Serializer\XmlRoot("ConfirmCredentials")
 */
class ConfirmCredentials extends AbstractEnvelope
{
    /**
     * @Serializer\SerializedName("User")
     * @Serializer\Type(ConfirmCredentialsUser::class)
     * @Serializer\Accessor(getter="getUser")
     */
    public ?ConfirmCredentialsUser $user = null;

    /**
     * @return ConfirmCredentialsUser
     */
    public function getUser(): ?ConfirmCredentialsUser
    {
        return $this->user;
    }
}
