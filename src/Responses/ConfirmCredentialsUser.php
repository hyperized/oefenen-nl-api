<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Responses;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Close
 * @package Hyperized\OefenenNlApi\Responses
 * @Serializer\XmlRoot("User")
 */
class ConfirmCredentialsUser extends AbstractEnvelope
{
    /**
     * @Serializer\SerializedName("UserName")
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getUserName")
     */
    public string $userName = '';

    /**
     * @Serializer\SerializedName("Group")
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getGroup")
     */
    public string $group = '';

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }
}
