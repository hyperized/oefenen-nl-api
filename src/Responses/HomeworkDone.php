<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Responses;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Close
 * @package Hyperized\OefenenNlApi\Responses
 * @Serializer\XmlRoot("HomeworkDone")
 */
class HomeworkDone extends AbstractEnvelope
{
    /**
     * @Serializer\SerializedName("Result")
     * @Serializer\Type("boolean")
     * @Serializer\Accessor(getter="getResult")
     */
    public bool $result = false;

    /**
     * @return bool
     */
    public function getResult(): bool
    {
        return $this->result;
    }


}
