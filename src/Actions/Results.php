<?php declare(strict_types=1);


namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Requests\Results as ResultsRequest;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\Results as ResultsResponse;
use Hyperized\OefenenNlApi\Types\HttpBody;
use Hyperized\OefenenNlApi\Types\HttpMethod;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Query;
use Hyperized\OefenenNlApi\Types\ResponseClass;
use JMS\Serializer\Serializer;

/**
 * Class Results
 * @package Hyperized\OefenenNlApi\Actions
 */
final class Results extends AbstractAction
{
    /**
     * Results constructor.
     * @param HttpClientInterface $client
     * @param Serializer $serializer
     * @param ProgramKey $programKey
     * @param ResultsRequest $results
     */
    protected function __construct(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        ResultsRequest $results
    )
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->responseClass = ResponseClass::fromString(ResultsResponse::class);

        $this->httpMethod = HttpMethod::fromString('POST');
        $this->query = Query::fromArray([
            'Action' => 'Results',
            'ProgramKey' => $programKey->getValue(),
        ]);
        $this->stream = HttpBody::fromString(
            self::serialize(
                $this->serializer,
                $results
            )
        );
    }

    /**
     * @param HttpClientInterface $client
     * @param Serializer $serializer
     * @param ProgramKey $programKey
     * @param ResultsRequest $results
     * @return static
     */
    public static function new(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        ResultsRequest $results
    ): self
    {
        return new Results($client, $serializer, $programKey, $results);
    }

    public static function validate(AbstractEnvelope $response): AbstractEnvelope
    {
        if (!($response instanceof ResultsResponse)) {
            throw InvalidArgumentException::incorrectResponseType();
        }

        if (!$response->getResult()) {
            throw InvalidArgumentException::resultsFailed();
        }

        return $response;
    }
}
