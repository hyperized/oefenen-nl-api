<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\ConfirmCredentials as ConfirmCredentialsResponse;
use Hyperized\OefenenNlApi\Responses\ConfirmCredentialsUser;
use Hyperized\OefenenNlApi\Types\HttpMethod;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Query;
use Hyperized\OefenenNlApi\Types\ResponseClass;
use Hyperized\OefenenNlApi\Types\Ticket;
use Hyperized\OefenenNlApi\Types\UserKey;
use Hyperized\ValueObjects\Concretes\Strings\EmptyByteArray;
use JMS\Serializer\Serializer;

/**
 * Class ConfirmCredentials
 * @package Hyperized\OefenenNlApi\Actions
 */
final class ConfirmCredentials extends AbstractAction
{
    protected function __construct(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        Ticket $ticket,
        UserKey $userKey)
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->responseClass = ResponseClass::fromString(ConfirmCredentialsResponse::class);

        $this->httpMethod = HttpMethod::fromString('GET');
        $this->query = Query::fromArray([
            'Action' => 'ConfirmCredentials',
            'ProgramKey' => $programKey->getValue(),
            'Ticket' => $ticket->getValue(),
            'UserKey' => $userKey->getValue()
        ]);
        $this->stream = EmptyByteArray::fromString('');
    }

    public static function new(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        Ticket $ticket,
        UserKey $userKey): self
    {
        return new ConfirmCredentials($client, $serializer, $programKey, $ticket, $userKey);
    }

    public static function validate(AbstractEnvelope $response): AbstractEnvelope
    {
        if (!($response instanceof ConfirmCredentialsResponse)) {
            throw InvalidArgumentException::incorrectResponseType();
        }

        if (!($response->getUser() instanceof ConfirmCredentialsUser)) {
            throw InvalidArgumentException::confirmationFailed();
        }

        return $response;
    }
}
