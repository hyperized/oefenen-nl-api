<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\HomeworkDone as HomeworkDoneResponse;
use Hyperized\OefenenNlApi\Types\ChapterId;
use Hyperized\OefenenNlApi\Types\HttpMethod;
use Hyperized\OefenenNlApi\Types\ModuleId;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Query;
use Hyperized\OefenenNlApi\Types\ResponseClass;
use Hyperized\OefenenNlApi\Types\UserKey;
use Hyperized\ValueObjects\Concretes\Strings\EmptyByteArray;
use JMS\Serializer\Serializer;

/**
 * Class HomeworkDone
 * @package Hyperized\OefenenNlApi\Actions
 */
final class HomeworkDone extends AbstractAction
{
    protected function __construct(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        UserKey $userKey,
        ChapterId $chapterId,
        ModuleId $moduleId
    )
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->responseClass = ResponseClass::fromString(HomeworkDoneResponse::class);

        $this->httpMethod = HttpMethod::fromString('GET');
        $this->query = Query::fromArray([
            'Action' => 'HomeworkDone',
            'ProgramKey' => $programKey->getValue(),
            'UserKey' => $userKey->getValue(),
            'ChapterID' => $chapterId->getValue(),
            'ModuleID' => $moduleId->getValue()
        ]);
        $this->stream = EmptyByteArray::fromString('');
    }

    /** @noinspection PhpTooManyParametersInspection */
    public static function new(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        UserKey $userKey,
        ChapterId $chapterId,
        ModuleId $moduleId
    ): self
    {
        return new HomeworkDone($client, $serializer, $programKey, $userKey, $chapterId, $moduleId);
    }

    public static function validate(AbstractEnvelope $response): AbstractEnvelope
    {
        if (!($response instanceof HomeworkDoneResponse)) {
            throw InvalidArgumentException::incorrectResponseType();
        }

        if (!$response->getResult()) {
            throw InvalidArgumentException::homeworkDoneFailed();
        }
        return $response;
    }
}
