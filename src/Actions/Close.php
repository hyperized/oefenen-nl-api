<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\Close as CloseResponse;
use Hyperized\OefenenNlApi\Types\HttpMethod;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Query;
use Hyperized\OefenenNlApi\Types\ResponseClass;
use Hyperized\OefenenNlApi\Types\Ticket;
use Hyperized\ValueObjects\Concretes\Strings\EmptyByteArray;
use JMS\Serializer\SerializerInterface;

/**
 * Class Close
 * @package Hyperized\OefenenNlApi\Actions
 */
final class Close extends AbstractAction
{
    protected function __construct(
        HttpClientInterface $client,
        SerializerInterface $serializer,
        ProgramKey $programKey,
        Ticket $ticket)
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->responseClass = ResponseClass::fromString(CloseResponse::class);

        $this->httpMethod = HttpMethod::fromString('GET');
        $this->query = Query::fromArray([
            'Action' => 'Close',
            'ProgramKey' => $programKey->getValue(),
            'Ticket' => $ticket->getValue()
        ]);
        $this->stream = EmptyByteArray::fromString('');
    }

    public static function new(
        HttpClientInterface $client,
        SerializerInterface $serializer,
        ProgramKey $programKey,
        Ticket $ticket): self
    {
        return new Close($client, $serializer, $programKey, $ticket);
    }

    /**
     * @param AbstractEnvelope $response
     * @return AbstractEnvelope
     * @throws InvalidArgumentException
     */
    public static function validate(AbstractEnvelope $response): AbstractEnvelope
    {
        if (!($response instanceof CloseResponse)) {
            throw InvalidArgumentException::incorrectResponseType();
        }

        if (!$response->getResult()) {
            throw InvalidArgumentException::closeFailed();
        }

        return $response;
    }
}
