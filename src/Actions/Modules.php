<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Requests\Modules as ModulesRequest;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Responses\Modules as ModulesResponse;
use Hyperized\OefenenNlApi\Types\HttpBody;
use Hyperized\OefenenNlApi\Types\HttpMethod;
use Hyperized\OefenenNlApi\Types\ProgramKey;
use Hyperized\OefenenNlApi\Types\Query;
use Hyperized\OefenenNlApi\Types\ResponseClass;
use JMS\Serializer\Serializer;

/**
 * Class Modules
 * @package Hyperized\OefenenNlApi\Actions
 */
final class Modules extends AbstractAction
{
    /**
     * Modules constructor.
     * @param HttpClientInterface $client
     * @param Serializer $serializer
     * @param ProgramKey $programKey
     * @param ModulesRequest $modules
     */
    protected function __construct(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        ModulesRequest $modules
    )
    {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->responseClass = ResponseClass::fromString(ModulesResponse::class);

        $this->httpMethod = HttpMethod::fromString('POST');
        $this->query = Query::fromArray([
            'Action' => 'Modules',
            'ProgramKey' => $programKey->getValue(),
        ]);
        $this->stream = HttpBody::fromString(
            self::serialize(
                $this->serializer,
                $modules
            )
        );
    }

    /**
     * @param HttpClientInterface $client
     * @param Serializer $serializer
     * @param ProgramKey $programKey
     * @param ModulesRequest $modules
     * @return static
     */
    public static function new(
        HttpClientInterface $client,
        Serializer $serializer,
        ProgramKey $programKey,
        ModulesRequest $modules
    ): self
    {
        return new Modules($client, $serializer, $programKey, $modules);
    }

    /**
     * @param AbstractEnvelope $response
     * @return AbstractEnvelope
     */
    public static function validate(AbstractEnvelope $response): AbstractEnvelope
    {
        if (!($response instanceof ModulesResponse)) {
            throw InvalidArgumentException::incorrectResponseType();
        }

        if (!$response->getResult()) {
            throw InvalidArgumentException::modulesFailed();
        }

        return $response;
    }
}
