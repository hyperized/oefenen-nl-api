<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Actions;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Utils;
use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Interfaces\ApiClientInterface;
use Hyperized\OefenenNlApi\Interfaces\DeserializerInterface;
use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;
use Hyperized\OefenenNlApi\Interfaces\HttpQueryInterface;
use Hyperized\OefenenNlApi\Interfaces\SerializerInterface;
use Hyperized\OefenenNlApi\Interfaces\ValidatorInterface;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use Hyperized\OefenenNlApi\Traits\DeserializableTrait;
use Hyperized\OefenenNlApi\Traits\SerializableTrait;
use Hyperized\OefenenNlApi\Types\HttpBody;
use Hyperized\OefenenNlApi\Types\Query;
use Hyperized\ValueObjects\Interfaces\Strings\ByteArrayInterface;
use JMS\Serializer\SerializerInterface as JmsSerializerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class Caller
 * @package Hyperized\OefenenNlApi\Abstracts
 */
abstract class AbstractAction implements
    ApiClientInterface,
    ValidatorInterface,
    DeserializerInterface,
    SerializerInterface
{
    use SerializableTrait;
    use DeserializableTrait;

    /**
     * @var HttpClientInterface
     */
    protected HttpClientInterface $client;
    /**
     * @var Query
     */
    protected Query $query;
    /**
     * @var JmsSerializerInterface
     */
    protected JmsSerializerInterface $serializer;
    /**
     * @var ByteArrayInterface
     */
    protected ByteArrayInterface $stream;
    /**
     * @var ByteArrayInterface
     */
    protected ByteArrayInterface $httpMethod;
    /**
     * @var ByteArrayInterface
     */
    protected ByteArrayInterface $responseClass;

    /**
     * Executes the call, deserialize & validation flow.
     * @return AbstractEnvelope
     */
    public function execute(): AbstractEnvelope
    {
        return
            static::validate(
                static::deserialize(
                    $this->serializer,
                    static::getResponseBody(
                        static::getResponse(
                            $this->client,
                            static::getRequest(
                                $this->httpMethod,
                                static::getPsr7Stream(
                                    $this->stream
                                )
                            ),
                            $this->query
                        )
                    )->getValue(),
                    $this->responseClass->getValue()
                )
            );
    }

    /**
     * @param ResponseInterface $response
     * @return ByteArrayInterface
     */
    public static function getResponseBody(ResponseInterface $response): ByteArrayInterface
    {
        return HttpBody::fromString(
            $response
                ->getBody()
                ->getContents()
        );
    }

    /**
     * @param HttpClientInterface $client
     * @param RequestInterface $request
     * @param HttpQueryInterface $query
     * @return ResponseInterface
     * @throws InvalidArgumentException
     */
    public static function getResponse(HttpClientInterface $client, RequestInterface $request, HttpQueryInterface $query): ResponseInterface
    {
        try {
            return $client
                ->getHttpClient()
                ->send(
                    $request,
                    [
                        'query' => $query->toArray()
                    ]
                );
        } catch (GuzzleException $exception) {
            throw InvalidArgumentException::apiFailed($exception);
        }
    }

    /**
     * Empty uri will be overwritten by base_uri from GuzzleClient
     * @param ByteArrayInterface $httpMethod
     * @param StreamInterface $body
     * @return Request
     */
    public static function getRequest(ByteArrayInterface $httpMethod, StreamInterface $body): RequestInterface
    {
        return new Request($httpMethod->getValue(), '', [], $body);
    }

    /**
     * @param ByteArrayInterface $body
     * @return StreamInterface
     */
    public static function getPsr7Stream(ByteArrayInterface $body): StreamInterface
    {
        return Utils::streamFor($body->getValue());
    }
}
