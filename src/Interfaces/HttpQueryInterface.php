<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Interfaces;

/**
 * Interface HttpQueryInterface
 * @package Hyperized\OefenenNlApi\Interfaces
 */
interface HttpQueryInterface
{
    /**
     * @param array $external
     * @return static
     */
    public static function fromArray(array $external = []): self;

    /**
     * @return array
     */
    public function toArray(): array;
}
