<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Interfaces;

use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;
use JMS\Serializer\SerializerInterface;

/**
 * Interface Deserializer
 * @package Hyperized\OefenenNlApi\Interfaces
 */
interface DeserializerInterface
{
    /**
     * @param SerializerInterface $serializer
     * @param string $data
     * @param string $type
     * @param string $deserializationMode
     * @return AbstractEnvelope
     */
    public static function deserialize(
        SerializerInterface $serializer,
        string $data,
        string $type,
        string $deserializationMode = 'xml'
    ): AbstractEnvelope;
}
