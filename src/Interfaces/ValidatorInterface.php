<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Interfaces;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\OefenenNlApi\Responses\AbstractEnvelope;

/**
 * Interface Validator
 * @package Hyperized\OefenenNlApi\Interfaces
 */
interface ValidatorInterface
{
    /**
     * Pass-through validator call
     * @param AbstractEnvelope $response
     * @return AbstractEnvelope
     * @throws InvalidArgumentException
     */
    public static function validate(AbstractEnvelope $response): AbstractEnvelope;
}
