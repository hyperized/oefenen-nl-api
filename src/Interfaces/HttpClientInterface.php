<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Interfaces;

use GuzzleHttp\Client as GuzzleClient;
use Hyperized\OefenenNlApi\Types\Url;

/**
 * Interface HttpClient
 * @package Hyperized\OefenenNlApi\Interfaces
 */
interface HttpClientInterface
{
    /**
     * @param Url $url
     * @return static
     */
    public static function new(Url $url): self;

    /**
     * @return GuzzleClient
     */
    public function getHttpClient(): GuzzleClient;
}
