<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Interfaces;

use Hyperized\ValueObjects\Interfaces\Strings\ByteArrayInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Interface Caller
 * @package Hyperized\OefenenNlApi\Interfaces
 */
interface ApiClientInterface
{
    /**
     * @param ByteArrayInterface $httpMethod
     * @param StreamInterface $body
     * @return RequestInterface
     */
    public static function getRequest(ByteArrayInterface $httpMethod, StreamInterface $body): RequestInterface;

    /**
     * @param ByteArrayInterface $body
     * @return StreamInterface
     */
    public static function getPsr7Stream(ByteArrayInterface $body): StreamInterface;

    /**
     * @param ResponseInterface $response
     * @return ByteArrayInterface
     */
    public static function getResponseBody(ResponseInterface $response): ByteArrayInterface;

    /**
     * @param HttpClientInterface $client
     * @param RequestInterface $request
     * @param HttpQueryInterface $query
     * @return ResponseInterface
     */
    public static function getResponse(HttpClientInterface $client, RequestInterface $request, HttpQueryInterface $query): ResponseInterface;

    /**
     * @return ResponseEnvelopeInterface
     */
    public function execute(): ResponseEnvelopeInterface;
}
