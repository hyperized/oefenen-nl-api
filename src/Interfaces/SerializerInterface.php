<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Interfaces;

use Hyperized\OefenenNlApi\Requests\AbstractEnvelope;
use JMS\Serializer\SerializerInterface as JmsSerializerInterface;

/**
 * Interface SerializerInterface
 * @package Hyperized\OefenenNlApi\Interfaces
 */
interface SerializerInterface
{
    /**
     * @param JmsSerializerInterface $serializer
     * @param AbstractEnvelope $data
     * @param string $serializationMode
     * @return string
     */
    public static function serialize(
        JmsSerializerInterface $serializer,
        AbstractEnvelope $data,
        string $serializationMode = 'xml'
    ): string;
}
