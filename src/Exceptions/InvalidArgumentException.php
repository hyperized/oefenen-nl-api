<?php declare(strict_types=1);


namespace Hyperized\OefenenNlApi\Exceptions;


use GuzzleHttp\Exception\GuzzleException;

/**
 * Class InvalidArgumentException
 * @package Hyperized\OefenenNlApi\Exceptions
 */
class InvalidArgumentException extends \InvalidArgumentException
{
    public static function incorrectResponseType(): InvalidArgumentException
    {
        return new self('Response type is incorrect.');
    }

    public static function invalidUrl(): InvalidArgumentException
    {
        return new self('Provided Base URL is not a valid URL.');
    }

    public static function apiFailed(GuzzleException $exception): InvalidArgumentException
    {
        return new self('API call returned invalid object: ' . $exception->getMessage() . '.');
    }

    public static function closeFailed(): InvalidArgumentException
    {
        return new self('API returned a False, should be a True.');
    }

    public static function confirmationFailed(): \InvalidArgumentException
    {
        return new self('Confirmation failed.');
    }

    public static function homeworkDoneFailed(): \InvalidArgumentException
    {
        return new self('Marking homework as done has failed.');
    }

    public static function modulesFailed(): InvalidArgumentException
    {
        return new self('API returned a False, should be a True.');
    }

    public static function resultsFailed(): InvalidArgumentException
    {
        return new self('API returned a False, should be a True.');
    }
}
