<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Integers\AbstractPositiveInteger;

/**
 * Class ExerciseDone
 * @package Hyperized\OefenenNlApi\Types
 */
class ExerciseDone extends AbstractPositiveInteger
{

}
