<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Integers\AbstractPositiveInteger;

/**
 * Class ExerciseCorrect
 * @package Hyperized\OefenenNlApi\Types
 */
class ExerciseCorrect extends AbstractPositiveInteger
{

}
