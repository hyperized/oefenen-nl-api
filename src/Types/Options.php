<?php /** @noinspection EmptyClassInspection */
declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use ArrayIterator;

/**
 * Class Options
 * @package Hyperized\OefenenNlApi\Types
 */
class Options extends ArrayIterator
{
}
