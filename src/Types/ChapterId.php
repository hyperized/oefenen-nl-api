<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Integers\AbstractPositiveInteger;

/**
 * Class ChapterId
 * @package Hyperized\OefenenNlApi\Types
 */
class ChapterId extends AbstractPositiveInteger
{

}
