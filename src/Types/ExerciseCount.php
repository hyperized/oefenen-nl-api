<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Integers\AbstractPositiveInteger;

/**
 * Class ExerciseCount
 * @package Hyperized\OefenenNlApi\Types
 */
class ExerciseCount extends AbstractPositiveInteger
{

}
