<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractNonEmptyByteArray;

/**
 * Class ProgramKey
 * @package Hyperized\OefenenNlApi\Types
 */
class ProgramKey extends AbstractNonEmptyByteArray
{

}
