<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractNonEmptyByteArray;

/**
 * Class ModuleTitle
 * @package Hyperized\OefenenNlApi\Types
 */
class ModuleTitle extends AbstractNonEmptyByteArray
{

}
