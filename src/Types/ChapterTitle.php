<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractNonEmptyByteArray;

/**
 * Class ChapterTitle
 * @package Hyperized\OefenenNlApi\Types
 */
class ChapterTitle extends AbstractNonEmptyByteArray
{

}
