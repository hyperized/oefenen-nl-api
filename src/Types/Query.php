<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\OefenenNlApi\Interfaces\HttpQueryInterface;

/**
 * Class Query
 * @package Hyperized\OefenenNlApi\Types
 */
final class Query implements HttpQueryInterface
{
    private array $internal;

    protected function __construct(array $external = [])
    {
        $this->internal = $external;
    }

    public static function fromArray(array $external = []): self
    {
        return new Query($external);
    }

    public function toArray(): array
    {
        return $this->internal;
    }
}
