<?php /** @noinspection PhpClassNamingConventionInspection */
declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\OefenenNlApi\Exceptions\InvalidArgumentException;
use Hyperized\ValueObjects\Abstracts\Strings\AbstractByteArray;

/**
 * Class Url
 * @package Hyperized\OefenenNlApi\Types
 */
class Url extends AbstractByteArray
{
    protected static function validate(string $value): void
    {
        parent::validate($value);

        if (filter_var($value, FILTER_VALIDATE_URL) === false) {
            throw InvalidArgumentException::invalidUrl();
        }
    }
}
