<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractNonEmptyByteArray;

/**
 * Class Ticket
 * @package Hyperized\OefenenNlApi\Types
 */
class Ticket extends AbstractNonEmptyByteArray
{

}
