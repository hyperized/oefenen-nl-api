<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractNonEmptyByteArray;

/**
 * Class UserKey
 * @package Hyperized\OefenenNlApi\Types
 */
class UserKey extends AbstractNonEmptyByteArray
{

}
