<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Integers\AbstractPositiveInteger;

/**
 * Class Sequence
 * @package Hyperized\OefenenNlApi\Types
 */
class Sequence extends AbstractPositiveInteger
{

}
