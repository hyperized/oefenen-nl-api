<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Integers\AbstractPositiveInteger;

/**
 * Class ModuleId
 * @package Hyperized\OefenenNlApi\Types
 */
class ModuleId extends AbstractPositiveInteger
{

}
