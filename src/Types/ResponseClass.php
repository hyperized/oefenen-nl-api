<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractNonEmptyByteArray;

/**
 * Class ResponseClass
 * @package Hyperized\OefenenNlApi\Types
 */
class ResponseClass extends AbstractNonEmptyByteArray
{

}
