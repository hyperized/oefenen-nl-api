<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractNonEmptyByteArray;

/**
 * Class HttpMethod
 * @package Hyperized\OefenenNlApi\Types
 */
class HttpMethod extends AbstractNonEmptyByteArray
{

}
