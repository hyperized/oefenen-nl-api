<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Types;

use Hyperized\ValueObjects\Abstracts\Strings\AbstractByteArray;

/**
 * Class HttpBody
 * @package Hyperized\OefenenNlApi\Types
 */
class HttpBody extends AbstractByteArray
{

}
