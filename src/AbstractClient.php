<?php /** @noinspection EmptyClassInspection */
declare(strict_types=1);

namespace Hyperized\OefenenNlApi;

use Hyperized\OefenenNlApi\Interfaces\HttpClientInterface;

/**
 * Class AbstractClient
 * @package Hyperized\OefenenNlApi
 */
abstract class AbstractClient implements HttpClientInterface
{
}
