<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Requests;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Results
 * @package Hyperized\OefenenNlApi\Responses
 * @Serializer\XmlRoot("Modules")
 */
class Modules extends AbstractEnvelope
{
    /**
     * @Serializer\XmlList(inline=true, entry="Module")
     * @Serializer\Type("array<Hyperized\OefenenNlApi\Requests\Module>")
     */
    public array $modules = [];

    /**
     * Modules constructor.
     * @param array $modules
     */
    protected function __construct(array $modules)
    {
        $this->modules = $modules;
    }

    /**
     * @param Module ...$modules
     * @return static
     */
    public static function new(Module ...$modules): self
    {
        return new static($modules);
    }
}
