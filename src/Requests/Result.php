<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Requests;

use Hyperized\OefenenNlApi\Types\ChapterId;
use Hyperized\OefenenNlApi\Types\ExerciseCorrect;
use Hyperized\OefenenNlApi\Types\ExerciseDone;
use Hyperized\OefenenNlApi\Types\ExerciseTotal;
use Hyperized\OefenenNlApi\Types\ModuleId;
use Hyperized\OefenenNlApi\Types\Url;
use Hyperized\OefenenNlApi\Types\UserKey;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\XmlElement;

/**
 * Class Result
 * @package Hyperized\OefenenNlApi\Requests
 */
class Result extends AbstractEnvelope
{
    /**
     * @Serializer\SerializedName("UserKey")
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getUserKey")
     * @XmlElement(cdata=false)
     */
    public UserKey $userKey;
    /**
     * @Serializer\SerializedName("ChapterID")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getChapterId")
     */
    public ChapterId $chapterId;
    /**
     * @Serializer\SerializedName("ModuleID")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getModuleId")
     */
    public ModuleId $moduleId;
    /**
     * @Serializer\SerializedName("ExerciseTotal")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getExerciseTotal")
     */
    public ExerciseTotal $exerciseTotal;
    /**
     * @Serializer\SerializedName("ExerciseDone")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getExerciseDone")
     */
    public ExerciseDone $exerciseDone;
    /**
     * @Serializer\SerializedName("ExerciseCorrect")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getExerciseCorrect")
     */
    public ExerciseCorrect $exerciseCorrect;
    /**
     * @Serializer\SerializedName("MoreInfoURL")
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getMoreInfoUrl")
     * @XmlElement(cdata=false)
     */
    public Url $moreInfoUrl;

    protected function __construct(
        UserKey $userKey,
        ChapterId $chapterId,
        ModuleId $moduleId,
        ExerciseTotal $exerciseTotal,
        ExerciseDone $exerciseDone,
        ExerciseCorrect $exerciseCorrect,
        Url $moreInfoUrl
    )
    {
        $this->userKey = $userKey;
        $this->chapterId = $chapterId;
        $this->moduleId = $moduleId;
        $this->exerciseTotal = $exerciseTotal;
        $this->exerciseDone = $exerciseDone;
        $this->exerciseCorrect = $exerciseCorrect;
        $this->moreInfoUrl = $moreInfoUrl;
    }

    /** @noinspection PhpTooManyParametersInspection */
    public static function new(
        UserKey $userKey,
        ChapterId $chapterId,
        ModuleId $moduleId,
        ExerciseTotal $exerciseTotal,
        ExerciseDone $exerciseDone,
        ExerciseCorrect $exerciseCorrect,
        Url $moreInfoUrl
    ): Result
    {
        return new static (
            $userKey,
            $chapterId,
            $moduleId,
            $exerciseTotal,
            $exerciseDone,
            $exerciseCorrect,
            $moreInfoUrl
        );
    }

    public function getExerciseTotal(): int
    {
        return $this->exerciseTotal->getValue();
    }

    public function getExerciseDone(): int
    {
        return $this->exerciseDone->getValue();
    }

    public function getExerciseCorrect(): int
    {
        return $this->exerciseCorrect->getValue();
    }

    public function getMoreInfoUrl(): string
    {
        return $this->moreInfoUrl->getValue();
    }

    public function getUserKey(): string
    {
        return $this->userKey->getValue();
    }

    public function getChapterId(): int
    {
        return $this->chapterId->getValue();
    }

    public function getModuleId(): int
    {
        return $this->moduleId->getValue();
    }
}
