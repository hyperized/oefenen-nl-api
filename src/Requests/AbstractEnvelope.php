<?php /** @noinspection EmptyClassInspection */
declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Requests;

use Hyperized\OefenenNlApi\Interfaces\RequestEnvelopeInterface;

/**
 * Class AbstractEnvelope
 * @package Hyperized\OefenenNlApi\Responses
 */
abstract class AbstractEnvelope implements RequestEnvelopeInterface
{
}