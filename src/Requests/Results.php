<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Requests;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Results
 * @package Hyperized\OefenenNlApi\Requests
 * @Serializer\XmlRoot("Results")
 */
class Results extends AbstractEnvelope
{
    /**
     * @Serializer\XmlList(inline=true, entry="Result")
     * @Serializer\Type("array<Hyperized\OefenenNlApi\Requests\Result>")
     */
    public array $results = [];

    /**
     * Results constructor.
     * @param array $results
     */
    protected function __construct(array $results)
    {
        $this->results = $results;
    }

    /**
     * @param Result ...$results
     * @return static
     */
    public static function new(Result ...$results): self
    {
        return new static($results);
    }
}
