<?php declare(strict_types=1);

namespace Hyperized\OefenenNlApi\Requests;

use Hyperized\OefenenNlApi\Types\ChapterId;
use Hyperized\OefenenNlApi\Types\ChapterTitle;
use Hyperized\OefenenNlApi\Types\ExerciseCount;
use Hyperized\OefenenNlApi\Types\ModuleId;
use Hyperized\OefenenNlApi\Types\ModuleTitle;
use Hyperized\OefenenNlApi\Types\Sequence;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\XmlElement;

/**
 * Class Module
 * @package Hyperized\OefenenNlApi\Requests
 */
class Module extends AbstractEnvelope
{
    /**
     * @Serializer\SerializedName("ModuleID")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getModuleId")
     */
    public ModuleId $moduleId;
    /**
     * @Serializer\SerializedName("ModuleTitle")
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getModuleTitle")
     * @XmlElement(cdata=false)
     */
    public ModuleTitle $moduleTitle;
    /**
     * @Serializer\SerializedName("ChapterID")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getChapterId")
     */
    public ChapterId $chapterId;
    /**
     * @Serializer\SerializedName("ChapterTitle")
     * @Serializer\Type("string")
     * @Serializer\Accessor(getter="getChapterTitle")
     * @XmlElement(cdata=false)
     */
    public ChapterTitle $chapterTitle;
    /**
     * @Serializer\SerializedName("ExerciseCount")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getExerciseCount")
     */
    public ExerciseCount $exerciseCount;
    /**
     * @Serializer\SerializedName("Sequence")
     * @Serializer\Type("int")
     * @Serializer\Accessor(getter="getSequence")
     */
    public Sequence $sequence;

    protected function __construct(
        ModuleId $moduleId,
        ModuleTitle $moduleTitle,
        ChapterId $chapterId,
        ChapterTitle $chapterTitle,
        ExerciseCount $exerciseCount,
        Sequence $sequence
    )
    {
        $this->moduleId = $moduleId;
        $this->moduleTitle = $moduleTitle;
        $this->chapterId = $chapterId;
        $this->chapterTitle = $chapterTitle;
        $this->exerciseCount = $exerciseCount;
        $this->sequence = $sequence;
    }

    /** @noinspection PhpTooManyParametersInspection */
    public static function new(
        ModuleId $moduleId,
        ModuleTitle $moduleTitle,
        ChapterId $chapterId,
        ChapterTitle $chapterTitle,
        ExerciseCount $exerciseCount,
        Sequence $sequence
    ): Module
    {
        return new static (
            $moduleId,
            $moduleTitle,
            $chapterId,
            $chapterTitle,
            $exerciseCount,
            $sequence
        );
    }

    public function getChapterId(): int
    {
        return $this->chapterId->getValue();
    }

    public function getChapterTitle(): string
    {
        return $this->chapterTitle->getValue();
    }

    public function getModuleId(): int
    {
        return $this->moduleId->getValue();
    }

    public function getModuleTitle(): string
    {
        return $this->moduleTitle->getValue();
    }

    public function getExerciseCount(): int
    {
        return $this->exerciseCount->getValue();
    }

    public function getSequence(): int
    {
        return $this->sequence->getValue();
    }


}